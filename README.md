# cirrus

![le logo cirrus](https://awebsome.fr/assets/images/l/2021-cirrus-logo-2.png)

Un cirrus est un type de nuage ayant hérité son nom latin de la forme qu'il rappelle, celle d'une boucle de cheveux. Au-delà de l'aspect poétique, cirrus est l'acronyme idéal du projet de Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces porté par [Awebsome](https://awebsome.fr).

Pour installer cirrus et apprendre à l'utiliser, rendez-vous sur :  
[https://getcirrus.awebsome.fr/](https://getcirrus.awebsome.fr/)

Ce projet est testé avec BrowserStack.  
*This project is tested with BrowserStack.*