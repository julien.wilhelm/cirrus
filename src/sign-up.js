document.querySelector('form').onsubmit = e => {
	if(e.target.elements['user-pass'].value !== e.target.elements['user-pass-conf'].value) {
		alert('Le mot de passe et la confirmation saisis ne correspondent pas.');
		e.preventDefault();
	}
};