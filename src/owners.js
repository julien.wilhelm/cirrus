/* ### PART 1: MVC CLASSES ### */

/* ### PART 2: BUILDING USER INTERFACE ### */

/* main nav */

ui.lNav.toRecycleBt = document.createElement('button');
ui.lNav.toRecycleBt.setAttribute('class', 'data-ft');
ui.lNav.toRecycleBt.setAttribute('title', 'Afficher la corbeille');
ui.lNav.toRecycleBt.innerHTML = '<svg viewBox="0 0 24 24"><path d="M9 19c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5-17v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.315c0 .901.73 2 1.631 2h5.712zm-3 4v16h-14v-16h-2v18h18v-18h-2z"/></svg>';
ui.lNav.toRecycleBt.onclick = () => Controller.browseDirectory('../../data/recycle');
ui.lNav.insertAdjacentElement('afterbegin', ui.lNav.toRecycleBt);

ui.lNav.toDataBt = document.createElement('button');
ui.lNav.toDataBt.setAttribute('class', 'recycle-ft');
ui.lNav.toDataBt.setAttribute('title', 'Afficher les données');
ui.lNav.toDataBt.innerHTML = '<svg viewBox="0 0 24 24"><path d="M12 5c3.453 0 5.891 2.797 5.567 6.78 1.745-.046 4.433.751 4.433 3.72 0 1.93-1.57 3.5-3.5 3.5h-13c-1.93 0-3.5-1.57-3.5-3.5 0-2.797 2.479-3.833 4.433-3.72-.167-4.218 2.208-6.78 5.567-6.78zm0-2c-4.006 0-7.267 3.141-7.479 7.092-2.57.463-4.521 2.706-4.521 5.408 0 3.037 2.463 5.5 5.5 5.5h13c3.037 0 5.5-2.463 5.5-5.5 0-2.702-1.951-4.945-4.521-5.408-.212-3.951-3.473-7.092-7.479-7.092z"/></svg>';
ui.lNav.toDataBt.onclick = () => Controller.browseDirectory('../../data/content');
ui.lNav.appendChild(ui.lNav.toDataBt);

ui.lNav.emptyRecycleBt = document.createElement('button');
ui.lNav.emptyRecycleBt.setAttribute('class', 'recycle-ft');
ui.lNav.emptyRecycleBt.setAttribute('title', 'Vider la corbeille (maintenir pour enclencher)');
ui.lNav.emptyRecycleBt.innerHTML = '<svg viewBox="0 0 24 24"><path d="M18.5 15c-2.486 0-4.5 2.015-4.5 4.5s2.014 4.5 4.5 4.5c2.484 0 4.5-2.015 4.5-4.5s-2.016-4.5-4.5-4.5zm-.469 6.484l-1.688-1.637.695-.697.992.94 2.115-2.169.697.696-2.811 2.867zm-2.031-12.484v4.501c-.748.313-1.424.765-2 1.319v-5.82c0-.552.447-1 1-1s1 .448 1 1zm-4 0v10c0 .552-.447 1-1 1s-1-.448-1-1v-10c0-.552.447-1 1-1s1 .448 1 1zm1.82 15h-11.82v-18h2v16h8.502c.312.749.765 1.424 1.318 2zm-6.82-16c.553 0 1 .448 1 1v10c0 .552-.447 1-1 1s-1-.448-1-1v-10c0-.552.447-1 1-1zm14-4h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711v2zm-1 2v7.182c-.482-.115-.983-.182-1.5-.182l-.5.025v-7.025h2z"/></svg>';
ui.lNav.emptyRecycleBt.onkeypress = () => {
	if(confirm('Vider la corbeille ?')){
		PubController.removeItem('RECYCLE', true);
	}	
};
ui.lNav.emptyRecycleBt.onmousedown = () => Controller.watchConfirm('start', 'recycle');
ui.lNav.emptyRecycleBt.onmouseup = () => {
	Controller.watchConfirm('end', 'recycle');
	PubController.removeItem('RECYCLE');
};
ui.lNav.emptyRecycleBt.onmouseleave = () => Controller.cancelConfirm();
ui.lNav.emptyRecycleBt.ontouchstart = e => Controller.watchConfirm('start', 'recycle', e);
ui.lNav.emptyRecycleBt.ontouchend = () => {
	Controller.watchConfirm('end', 'recycle');
	PubController.removeItem('RECYCLE');
};
ui.lNav.emptyRecycleBt.ontouchmove = () => Controller.cancelConfirm();
ui.lNav.appendChild(ui.lNav.emptyRecycleBt);

ui.rNav.toAdminBt = document.createElement('button');
ui.rNav.toAdminBt.setAttribute('title', 'Accéder aux outils d\'administration');
ui.rNav.toAdminBt.innerHTML = '<svg viewBox="0 0 24 24"><path d="M24 14.187v-4.374c-2.148-.766-2.726-.802-3.027-1.529-.303-.729.083-1.169 1.059-3.223l-3.093-3.093c-2.026.963-2.488 1.364-3.224 1.059-.727-.302-.768-.889-1.527-3.027h-4.375c-.764 2.144-.8 2.725-1.529 3.027-.752.313-1.203-.1-3.223-1.059l-3.093 3.093c.977 2.055 1.362 2.493 1.059 3.224-.302.727-.881.764-3.027 1.528v4.375c2.139.76 2.725.8 3.027 1.528.304.734-.081 1.167-1.059 3.223l3.093 3.093c1.999-.95 2.47-1.373 3.223-1.059.728.302.764.88 1.529 3.027h4.374c.758-2.131.799-2.723 1.537-3.031.745-.308 1.186.099 3.215 1.062l3.093-3.093c-.975-2.05-1.362-2.492-1.059-3.223.3-.726.88-.763 3.027-1.528zm-4.875.764c-.577 1.394-.068 2.458.488 3.578l-1.084 1.084c-1.093-.543-2.161-1.076-3.573-.49-1.396.581-1.79 1.693-2.188 2.877h-1.534c-.398-1.185-.791-2.297-2.183-2.875-1.419-.588-2.507-.045-3.579.488l-1.083-1.084c.557-1.118 1.066-2.18.487-3.58-.579-1.391-1.691-1.784-2.876-2.182v-1.533c1.185-.398 2.297-.791 2.875-2.184.578-1.394.068-2.459-.488-3.579l1.084-1.084c1.082.538 2.162 1.077 3.58.488 1.392-.577 1.785-1.69 2.183-2.875h1.534c.398 1.185.792 2.297 2.184 2.875 1.419.588 2.506.045 3.579-.488l1.084 1.084c-.556 1.121-1.065 2.187-.488 3.58.577 1.391 1.689 1.784 2.875 2.183v1.534c-1.188.398-2.302.791-2.877 2.183zm-7.125-5.951c1.654 0 3 1.346 3 3s-1.346 3-3 3-3-1.346-3-3 1.346-3 3-3zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5z"/></svg>';
ui.rNav.toAdminBt.onclick = () => window.location.href = './pages/admin';
document.getElementById('toggleDarkThemeBt').insertAdjacentElement('afterend',(ui.rNav.toAdminBt));

/* ### PART 3: ENDING PROCEDURAL ### */

document.body.classList.add('--is-owner');