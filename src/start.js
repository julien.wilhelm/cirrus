Controller.openURL();
window.onpopstate = () => Controller.openURL();
if(localStorage.getItem('dark-theme') === 'off') View.unsetDarkTheme();
ui.main.classList.add('home-main', '--visible');