"use strict";

/* ### PART 1: MVC CLASSES ### */

class Model {

	static ajaxPost = query => {
		return new Promise(
			(resolve, reject) => {
				let formData = new FormData();
				if(query.args !== undefined) {
					for(const arg of query.args) {
						formData.append(arg.name, arg.value);
					}
				}
				fetch(query.script, {
					method: 'POST',
					body: formData
				})
				.then(data => {					
					if(data.ok) {
						data.text()
						.then(data => {
							try {
								resolve(JSON.parse(data));
							}
							catch(e) {
								reject(data);
							}	
						});
					}
					else {
						reject(data);
					}
				});
			}
		);
	};

};

class View {

	static setDarkTheme = () => {
		document.body.classList.add('--dark-theme');
		localStorage.removeItem('dark-theme');
	};

	static unsetDarkTheme = () => {
		document.body.classList.remove('--dark-theme');
		localStorage.setItem('dark-theme', 'off');			
	};

	static toggleDarkTheme = () => document.body.classList.contains('--dark-theme') ? 
		View.unsetDarkTheme(): 
		View.setDarkTheme();

	static setList = (listId, content = null) => {
		const listElm = document.getElementById(listId);
		listElm.innerHTML = '';
		if(content !== null) {
			if(listId === 'logs__list') {
				for(const log of content) {
					if(log !== '') {
						let itemElm = document.createElement('li');
						itemElm.textContent = '[' + log;
						listElm.appendChild(itemElm);
					}
				}
			}
			else {
				for(let a of content) {
					let itemElm = document.createElement('li');
					let aElm = document.createElement('a');
					aElm.setAttribute('href', a);
					aElm.setAttribute('target', '_blank');
					aElm.textContent = a;
					itemElm.appendChild(aElm);
					listElm.appendChild(itemElm);
				}
			}
		}
	};

	static setUserList = users => {
		const listElm = document.getElementById('users__list');
		listElm.innerHTML = '';
		if(users.length > 0) {
			for(let user of users) {
				let itemElm = document.createElement('li');
				let setUserBtElm = document.createElement('button');
				if(user.role === 'viewer') {
					itemElm.innerHTML = `${user.name} (<em>rôle actuel : visualiseur</em>)`;
					setUserBtElm.textContent = 'Changer en éditeur';
					setUserBtElm.onclick = () => Controller.moveUser(user.name, 'viewer', 'publisher');
				}
				else if(user.role === 'publisher') {
					itemElm.innerHTML = `${user.name} (<em>rôle actuel : éditeur</em>)`;
					setUserBtElm.textContent = 'Transformer en visualiseur';
					setUserBtElm.onclick = () => Controller.moveUser(user.name, 'publisher', 'viewer');
				}
				itemElm.appendChild(setUserBtElm);
				let delBtElm = document.createElement('button');
				delBtElm.textContent = 'Supprimer le compte';
				delBtElm.onclick = () => Controller.removeUser(user.name, user.role);
				itemElm.appendChild(delBtElm);
				listElm.appendChild(itemElm);
			}
		}
	};

};

class Controller {

	static browseUsers = () => {
		Model.ajaxPost({script: '../../app/server/adm-browse-users.php'})
		.then(data => View.setUserList(data.content));
	};

	static moveUser = (userName, fromRole, toRole) => {
		Model.ajaxPost(
			{ 
				script: '../../app/server/adm-move-user.php',
				args: [
					{
						name: 'user-name',
						value: userName
					},
					{
						name: 'from-role',
						value: fromRole
					},
					{
						name: 'to-role',
						value: toRole
					}
				]
			}
		)
		.then(() => Controller.browseUsers());
	};

	static removeUser = (userName, userRole) => {
		if(confirm('Supprimer le compte : ' + userName)) {
			Model.ajaxPost(
				{ 
					script: '../../app/server/adm-remove-user.php',
					args: [
						{
							name: 'user-name',
							value: userName
						},
						{
							name: 'user-role',
							value: userRole
						}
					]
				}
			)
			.then(() => Controller.browseUsers());
		}
	};

	static manageVisitors = action => {
		Model.ajaxPost(
			{
				script: '../../app/server/adm-manage-visitors.php',
				args: [
					{
						name: 'action',
						value: action
					}
				]			
			}
		)
		.then(data => View.setList('visitors__list', data.content));
	};

	static manageInvitations = (action, role) => {
		Model.ajaxPost(
			{
				script: '../../app/server/adm-manage-invitations.php',
				args: [
					{
						name: 'role',
						value: role
					},
					{
						name: 'action',
						value: action
					}
				]	
			}
		)
		.then(data => View.setList(role + '__list', data.content));
	};

};

/* ### PART 2: BUILDING USER INTERFACE ### */

const ui = {};
ui.main = document.createElement('main');

/* main nav */

ui.nav = document.createElement('nav');
ui.nav.setAttribute('class', 'nav');

ui.lNav = document.createElement('div');
ui.nav.append(ui.lNav);

ui.rNav = document.createElement('div');
ui.rNav.setAttribute('class', 'nav__right-ct');

ui.rNav.toggleDarkThemeBt = document.createElement('button');
ui.rNav.toggleDarkThemeBt.setAttribute('id', 'toggleDarkThemeBt');
ui.rNav.toggleDarkThemeBt.setAttribute('title', 'Basculer entre thème clair / thème sombre');
ui.rNav.toggleDarkThemeBt.innerHTML = '<svg viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10v-20zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12z"/></svg>';
ui.rNav.toggleDarkThemeBt.onclick = () => View.toggleDarkTheme();
ui.rNav.appendChild(ui.rNav.toggleDarkThemeBt);

ui.rNav.logOutBt = document.createElement('button');
ui.rNav.logOutBt.setAttribute('title', 'Retour');
ui.rNav.logOutBt.innerHTML = '<svg viewBox="0 0 24 24"><path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z"/></svg>';
ui.rNav.logOutBt.onclick = () => window.location.href = '../../';
ui.rNav.appendChild(ui.rNav.logOutBt);

ui.nav.append(ui.rNav);

ui.main.appendChild(ui.nav);

/* visitors access */

ui.visitors = document.createElement('section');

ui.visitors.h2 = document.createElement('h2');
ui.visitors.h2.textContent =  'Lien d\'accès "visiteurs"';

ui.visitors.addBt = document.createElement('button');
ui.visitors.addBt.textContent = 'Activer le lien d\'accès';
ui.visitors.addBt.onclick = () => Controller.manageVisitors('create');

ui.visitors.delBt = document.createElement('button');
ui.visitors.delBt.textContent = 'Désactiver le lien d\'accès';
ui.visitors.delBt.onclick = () => Controller.manageVisitors('remove');

ui.visitors.list = document.createElement('ul');
ui.visitors.list.setAttribute('id', 'visitors__list');

ui.visitors.appendChild(ui.visitors.h2);
ui.visitors.appendChild(ui.visitors.addBt);
ui.visitors.appendChild(ui.visitors.delBt);
ui.visitors.appendChild(ui.visitors.list);
ui.main.appendChild(ui.visitors);

/* viewer invitations */

ui.viewers = document.createElement('section');

ui.viewers.h2 = document.createElement('h2');
ui.viewers.h2.textContent =  'Invitations "visualiseur"';

ui.viewers.addBt = document.createElement('button');
ui.viewers.addBt.textContent = 'Générer un lien d\'inscription';
ui.viewers.addBt.onclick = () => Controller.manageInvitations('create', 'viewer');

ui.viewers.delBt = document.createElement('button');
ui.viewers.delBt.textContent = 'Supprimer les invitations';
ui.viewers.delBt.onclick = () => Controller.manageInvitations('remove', 'viewer');

ui.viewers.list = document.createElement('ul');
ui.viewers.list.setAttribute('id', 'viewer__list');

ui.viewers.appendChild(ui.viewers.h2);
ui.viewers.appendChild(ui.viewers.addBt);
ui.viewers.appendChild(ui.viewers.delBt);
ui.viewers.appendChild(ui.viewers.list);
ui.main.appendChild(ui.viewers);

/* publisher invitations */

ui.publishers = document.createElement('section');

ui.publishers.h2 = document.createElement('h2');
ui.publishers.h2.textContent =  'Invitations "éditeur"';

ui.publishers.addBt = document.createElement('button');
ui.publishers.addBt.textContent = 'Générer un lien d\'inscription';
ui.publishers.addBt.onclick = () => Controller.manageInvitations('create', 'publisher');

ui.publishers.delBt = document.createElement('button');
ui.publishers.delBt.textContent = 'Supprimer les invitations';
ui.publishers.delBt.onclick = () => Controller.manageInvitations('remove', 'publisher');

ui.publishers.list = document.createElement('ul');
ui.publishers.list.setAttribute('id', 'publisher__list');

ui.publishers.appendChild(ui.publishers.h2);
ui.publishers.appendChild(ui.publishers.addBt);
ui.publishers.appendChild(ui.publishers.delBt);
ui.publishers.appendChild(ui.publishers.list);
ui.main.appendChild(ui.publishers);

/* user list */

ui.userList = document.createElement('section');

ui.userList.h2 = document.createElement('h2');
ui.userList.h2.textContent = 'Utilisateurs';

ui.userList.list = document.createElement('ul');
ui.userList.list.setAttribute('id', 'users__list');

ui.userList.appendChild(ui.userList.h2);
ui.userList.appendChild(ui.userList.list);
ui.main.appendChild(ui.userList);

/* footer */

ui.footer = document.createElement('footer');

ui.footer.docLink = document.createElement('a');
ui.footer.docLink.setAttribute('href', 'https://getcirrus.awebsome.fr/');
ui.footer.docLink.setAttribute('target', '_blank');
ui.footer.docLink.textContent = 'Besoin d\'aide ?';
ui.footer.appendChild(ui.footer.docLink);

ui.footer.userInfos = document.createElement('strong');
ui.footer.userInfos.textContent = `${userName} | ${userRole}`;
ui.footer.appendChild(ui.footer.userInfos);

ui.footer.version = document.createElement('span');
ui.footer.version.textContent = 'v.' + version;
ui.footer.appendChild(ui.footer.version);

ui.main.appendChild(ui.footer);

document.body.insertAdjacentElement('afterbegin', ui.main);

/* ### PART 3: ENDING PROCEDURAL ### */

Controller.manageVisitors('browse');
Controller.manageInvitations('browse','viewer');
Controller.manageInvitations('browse','publisher');
Controller.browseUsers();

if(localStorage.getItem('dark-theme') === 'off') {
	View.unsetDarkTheme();
}

ui.main.classList.add('--visible');