const gulp = require('gulp');
const concat = require('gulp-concat');
const minify = require('gulp-terser');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));

const srcPath = './src';
const destPath = './app/client';
const version = '1.9.2';

exports.build = () => {

	return new Promise((resolve) => {

		// JS

		gulp.src([`${srcPath}/viewers.js`, `${srcPath}/start.js`])
			.pipe(concat(`viewers-${version}.min.js`))
				.pipe(minify({ mangle: {toplevel: true }}))
					.pipe(gulp.dest(destPath));
	
		gulp.src([`${srcPath}/viewers.js`, `${srcPath}/publishers.js`, `${srcPath}/start.js`])
			.pipe(concat(`publishers-${version}.min.js`))
				.pipe(minify({ mangle: {toplevel: true }}))
					.pipe(gulp.dest(destPath));
		
		gulp.src([`${srcPath}/viewers.js`, `${srcPath}/publishers.js`, `${srcPath}/owners.js`, `${srcPath}/start.js`])
			.pipe(concat(`owners-${version}.min.js`))
				.pipe(minify({ mangle: {toplevel: true }}))
					.pipe(gulp.dest(destPath));

		gulp.src(`${srcPath}/admin.js`)
			.pipe(concat(`admin-${version}.min.js`))
				.pipe(minify({ mangle: {toplevel: true }}))
					.pipe(gulp.dest(destPath));
		
		gulp.src(`${srcPath}/sign-up.js`)
			.pipe(concat(`sign-up-${version}.min.js`))
				.pipe(minify({ mangle: {toplevel: true }}))
					.pipe(gulp.dest(destPath));

		// CSS

		gulp.src(`${srcPath}/style.sass`)
			.pipe(sass({outputStyle: 'compressed'}))
				.pipe(rename(`style-${version}.min.css`))
					.pipe(gulp.dest(destPath));
		
		resolve();
	
	});

}

exports.dev = () => {

	// JS

	gulp.watch([`${srcPath}/start.js`, `${srcPath}/viewers.js`, `${srcPath}/publishers.js`, `${srcPath}/owners.js`], () => {

		return new Promise((resolve) => {

			gulp.src([`${srcPath}/viewers.js`, `${srcPath}/start.js`])
				.pipe(concat(`viewers-${version}.min.js`))
					.pipe(gulp.dest(destPath));

			gulp.src([`${srcPath}/viewers.js`, `${srcPath}/publishers.js`, `${srcPath}/start.js`])
				.pipe(concat(`publishers-${version}.min.js`))
					.pipe(gulp.dest(destPath));

			gulp.src([`${srcPath}/viewers.js`, `${srcPath}/publishers.js`, `${srcPath}/owners.js`, `${srcPath}/start.js`])
				.pipe(concat(`owners-${version}.min.js`))
						.pipe(gulp.dest(destPath));
						
			resolve();

		});

	});

	gulp.watch(`${srcPath}/admin.js`, () => {

		return new Promise((resolve) => {
				
			gulp.src(`${srcPath}/admin.js`)
				.pipe(rename(`admin-${version}.min.js`))
					.pipe(gulp.dest(destPath));
					
			resolve();
		
		});

	});

	gulp.watch(`${srcPath}/sign-up.js`, () => {

		return new Promise((resolve) => {
				
			gulp.src(`${srcPath}/sign-up.js`)
				.pipe(rename(`sign-up-${version}.min.js`))
					.pipe(gulp.dest(destPath));
					
			resolve();

		});
	
	});

	// CSS

	gulp.watch('./src/*.sass', () => {

		return new Promise((resolve) => {

			gulp.src(`${srcPath}/style.sass`)
				.pipe(sass())
					.pipe(rename(`style-${version}.min.css`))
						.pipe(gulp.dest(destPath));

			resolve();

		});

	});

}