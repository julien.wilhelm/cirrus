<?php 
require_once '../../app/server/tools.php';
// Directly try to authenticate with the provided public auth.
if(isset($_GET['auth'])) { 
	header('Location: ' . getLocation('../../app/server/sign-in.php'));
	exit;
} 
// Page content ?>
<!DOCTYPE html>
	<html lang="fr">
	<head>
		<link rel="icon" href="../../app/client/cirrus-favicon.png">
		<link rel="stylesheet" href="../../app/client/style-<?= VERSION; ?>.min.css">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>cirrus | se connecter</title>
	</head>
	<body class="sign-page --dark-theme">
		<main>		
			<h1>cirrus | <span>se connecter</span></h1>
			<form action="<?= getLocation('../../app/server/sign-in.php'); ?>" method="POST">
				<?php if(isset($_GET['error'])) echo '<p style="color:#f44;">Échec de la connexion, vérifiez vos identifiants.</p>'; ?>
				<label>
					Nom d'utilisateur
					<input 
						id="user-name" 
						name="user-name" 
						required>
				</label>
				<label class="password">
					Mot de passe
					<input type="password" 
						id="user-pass" 
						name="user-pass" 
						required>
				</label>
				<button title="Se connecter" type="submit"><svg viewBox="0 0 24 24"><path d="M9 21.035l-9-8.638 2.791-2.87 6.156 5.874 12.21-12.436 2.843 2.817z"/></svg></button>
			</form>
			<img src="../../app/client/cirrus-cover.svg" alt="Logo cirrus">
		</main>
    </body>
</html>