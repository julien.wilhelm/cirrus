<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isOwner()) { ?>
	<!DOCTYPE html>
		<html lang="fr">
		<head>
			<link rel="icon" href="../../app/client/cirrus-favicon.png">
			<link rel="stylesheet" href="../../app/client/style-<?= VERSION; ?>.min.css">
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>cirrus | administration</title>
		</head>
		<body class="admin-page --dark-theme">
			<script>
				const version = '<?= VERSION; ?>';
				const userName = '<?= $userName; ?>';
				const userRole = '<?= $userRole; ?>';
			</script>
			<script src="../../app/client/admin-<?= VERSION; ?>.min.js"></script>
		</body>
	</html>
<?php }
else {
	header('Location: ../..');
	exit;
}