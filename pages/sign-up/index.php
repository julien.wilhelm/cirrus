<?php 
require_once '../../app/server/tools.php';
if(isset($_GET['role'], $_GET['auth'])) { 
	if($_GET['role'] === 'owner' || $_GET['role'] === 'publisher' || $_GET['role'] === 'viewer') {
		if(is_file('../../data/auth/sign-up-as-' . $_GET['role'] . '/' . $_GET['auth'])) { ?>
			<!DOCTYPE html>
				<html lang="fr">
				<head>
					<link rel="icon" href="../../app/client/cirrus-favicon.png">
					<link rel="stylesheet" href="../../app/client/style-<?= VERSION; ?>.min.css">
					<meta charset="UTF-8">
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<title>cirrus | créer un compte</title>
				</head>
				<body class="sign-page --dark-theme">
					<main>
						<h1>cirrus | <span>créer un compte</span></h1>
						<form action="../../app/server/sign-up.php" method="POST">
							<label>
								Nom d'utilisateur
								<span>8 à 16 lettres et/ou chiffres</span>
								<?php if(isset($_GET['error']) && $_GET['error'] === 'user-exists') {
									echo '<span style="color:#f44;">Nom d\'utilisateur déjà pris</span>';
								} ?>
								<input 
									id="user-name" 
									minlength="8"
									maxlength="16"
									name="user-name" 
									pattern="[a-zA-Z0-9]{8,16}" 
									required>
							</label>
							<label class="password">
								Mot de passe
								<span>8 à 24 lettres, chiffres et/ou * ? ! # $ @ %</span>
								<input type="password" 
									id="user-pass" 
									minlength="8"
									maxlength="24"
									name="user-pass" 
									pattern="[a-zA-Z0-9*?!#$@%]{8,24}" 
									required>
							</label>
							<label class="password">
								Confirmation mot de passe
								<input type="password" 
									id="user-pass-conf" 
									minlength="8"
									maxlength="24"
									name="user-pass-conf" 
									pattern="[a-zA-Z0-9*?!#$@%]{8,24}" 
									required>
							</label>
							<input
								type="hidden"
								name="role"
								value="<?= $_GET['role'] ?>">
							<input
								type="hidden"
								name="auth"
								value="<?= $_GET['auth'] ?>">
							<button title="Créer le compte" type="submit"><svg viewBox="0 0 24 24"><path d="M9 21.035l-9-8.638 2.791-2.87 6.156 5.874 12.21-12.436 2.843 2.817z"/></svg></button>
						</form>
						<img src="../../app/client/cirrus-cover.svg" alt="Logo cirrus">
					</main>
					<script src="../../app/client/sign-up-<?= VERSION; ?>.min.js"></script>
				</body>
			</html>
		<?php 
		}
		else {
			header('Location: ../..');
		}
	}
	else {
		header('Location: ../..');
	}
}
else {
	header('Location: ../..');
}