<?php 

require_once './app/server/tools.php';

// Is cirrus installed?
if(!is_dir('./data')) {
	header('Location: ./app/server/install.php');
	exit;
}

// Is user authenticated? 
if(!isAuthenticated()) { 
	header('Location: ' . getLocation(
		isset($_GET['auth']) ? 
			'../../app/server/sign-in.php': // Directly try to authenticate with the provided public auth.
			'./pages/sign-in/' // Redirects to the manual authentication page.
		)
	);			
	exit;
}

$publicAuthDir = scandir('./data/auth/sign-up-as-visitor');
$publicAuth = isset($publicAuthDir[2]) ? $publicAuthDir[2] : null; 

// Which script to use for the application?
if(isOwner()) $script = 'owners'; 
else if(isPublisher()) $script = 'publishers';
else $script = 'viewers'; 

// App starting content ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="icon" href="./app/client/cirrus-favicon.png">
		<link rel="stylesheet" href="./app/client/style-<?= VERSION; ?>.min.css">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>cirrus | Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces</title>
	</head>
	<body class="--dark-theme">
		<script>
			// Usefull JS globals.
			const version = '<?= VERSION; ?>';
			const userName = '<?= $userName; ?>';
			const userRole = '<?= $userRole; ?>';
			let url = ''; 
			let queryParams = {
				context: '', 
				dir: '', 
				file: '',
				auth : '<?= $publicAuth ?>' || undefined
			};
		</script>
		<script src="./app/client/<?= $script ?>-<?= VERSION ?>.min.js"></script>
	</body>
</html>