<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(isset($_POST['dirPath'], $_POST['isRestricted'], $_POST['accreditedUsers'])) {
		if(inDataDir($_POST['dirPath']) && ($_POST['isRestricted'] === 'true' || $_POST['isRestricted'] === 'false')) {
			// .lock file: restrict access directory
			// .perms file: allow some users to access directory
			$isRestricted = $_POST['isRestricted'] === 'true' ? true : false;
			$lockPath = $_POST['dirPath'] . '/.lock';
			$permsPath = $_POST['dirPath'] . '/.perms';
			if($isRestricted) {
				if(!is_file($lockPath)) {
					touch($lockPath);
				}
				if($_POST['accreditedUsers'] !== '') {
					$accreditedUsers = preg_split('/\r\n|[\r\n]/', $_POST['accreditedUsers']);
					file_put_contents($permsPath, json_encode($accreditedUsers));
				}
				else {
					if(file_exists($permsPath)) {
						unlink($permsPath);
					}
				}
			}
			else {
				if(is_file($lockPath)) {
					unlink($lockPath);
				}
				if(file_exists($permsPath)) {
					unlink($permsPath);
				}
			}
			echo json_encode (['success' => true]);
			return;
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);