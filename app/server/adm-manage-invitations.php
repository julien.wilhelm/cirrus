<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isOwner()) {
	if(isset($_POST['role'], $_POST['action'])) {
		if($_POST['role'] === 'publisher') {
			$authsDir = '../../data/auth/sign-up-as-publisher';
		}
		else if($_POST['role'] === 'viewer') {
			$authsDir = '../../data/auth/sign-up-as-viewer';
		}
        if(isset($authsDir)) {
            switch($_POST['action']) {
                case 'browse':
                case 'create':
                    if($_POST['action'] === 'create') {
                        $newAuth = hash('sha512', random_bytes(24));
                        touch($authsDir . '/' . $newAuth);                        
                    }
                    $auths = array_diff(scandir($authsDir), ['.', '..']);
                    $respContent = [];
                    if(count($auths) > 0) {
                        
                        foreach($auths as $auth) {
                            $respContent[] = '../../pages/sign-up/?role=' . $_POST['role'] . '&auth=' . $auth;
                        }
                    }
                    if(error_get_last() === null) {
                        echo json_encode (
                            [
                                'success' => true,
                                'content' => $respContent
                            ]
                        );
                    }
                    break;
                case 'remove':
                    $auths = array_diff(scandir($authsDir), ['.', '..']);
                    foreach($auths as $auth) {
                        unlink($authsDir . '/' . $auth);
                    }
                    if(error_get_last() === null) {
                        echo json_encode(['success' => true]);
                    }
                    break;
            }
        }
    }
}