<?php
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(isset($_POST['fromPath'], $_POST['toPath'])) {
		if(
			(inDataDir($_POST['fromPath']) && inDataDir($_POST['toPath']) && isPublisher()) ||
			(inRecycleDir($_POST['fromPath']) && inRecycleDir($_POST['toPath']) && isOwner())
		) {
			if(is_file($_POST['fromPath']) || is_link($_POST['fromPath']) || is_dir($_POST['fromPath'])) {
				if(rename($_POST['fromPath'], getValidPath($_POST['toPath']))) {
					echo json_encode(['success' => true]);
					return;
				}
				exit(ERRORS['failure']);
			}
			exit(ERRORS['missing']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);