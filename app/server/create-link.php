<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(isset($_POST['parentDir'], $_POST['label'], $_POST['href'])) {	
		if(inDataDir($_POST['parentDir'])) {
			$linkLabel = $_POST['label'];
			$linkHref = $_POST['href'];
			$linkPath = $_POST['parentDir'] . '/' . urlencode($linkLabel);
			if(is_link($linkPath)) {
				unlink($linkPath);
			}
			if(symlink($linkHref, $linkPath)) {
				echo json_encode(
					[
						'items' => [
							[
								'href' => $linkHref,
								'label' => $linkLabel,
								'path' => $linkPath,
								'type' => 'link'
							]
						]
					]
				);
				return;
			}
			exit(ERRORS['failure']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);