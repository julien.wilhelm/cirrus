<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(!empty($_FILES)) {	
		if(isset($_POST['parentDir']) && inDataDir($_POST['parentDir'])) {
			if(isset($_FILES) && $_FILES['file']['error'] === 0) {
				$fileName = getValidName($_FILES['file']['name']);
				$filePath = getValidPath($_POST['parentDir'] . '/' . $fileName); 
				$fileLabel = str_replace($_POST['parentDir'] . '/', '', $filePath);			
				if(move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
					echo json_encode(
						[
							'items' => [
								[
									'parentDir' => $_POST['parentDir'],
									'label' => $fileLabel,
									'path' => $filePath,
									'type' => 'file'
								]
							]
						]
					);
					return;
				}
				exit(ERRORS['failure']);
			}
			exit(ERRORS['failure']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['failure']);
}
exit(ERRORS['forbidden']);