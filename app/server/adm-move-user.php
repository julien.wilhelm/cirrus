<?php
require_once '../../app/server/tools.php';
if(isAuthenticated() && isOwner()) {
	if(isset($_POST['user-name'], $_POST['from-role'], $_POST['to-role'])) {
		if($_POST['from-role'] === 'viewer' && $_POST['to-role'] === 'publisher') {
			$fromPath = "../../data/users/viewers/{$_POST['user-name']}";
			$toPath = "../../data/users/publishers/{$_POST['user-name']}";
		}
		else if($_POST['from-role'] === 'publisher' && $_POST['to-role'] === 'viewer') {
			$fromPath = "../../data/users/publishers/{$_POST['user-name']}";
			$toPath = "../../data/users/viewers/{$_POST['user-name']}";
		}
		if(isset($fromPath, $toPath)) {
			if(rename($fromPath, $toPath)) {
				echo json_encode(['success' => true]);
			}
		}
	}
}