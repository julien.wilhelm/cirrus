<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(isset($_POST['parentDir'], $_POST['dirToCreate'])) {	
		if(inDataDir($_POST['parentDir'])) {
			$dirName = getValidName($_POST['dirToCreate']);
			$dirPath = getValidPath($_POST['parentDir'] . '/' . $dirName); 
			$dirLabel = str_replace($_POST['parentDir'] . '/', '', $dirPath);
			if(mkdir($dirPath, 0777, false)) {
				echo json_encode(
					[
						'items' => [
							[
								'label' => $dirLabel,
								'path' => $dirPath,
								'type' => 'subdir'
							]
						]
					]
				);
				return;
			}
			exit(ERRORS['failure']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);