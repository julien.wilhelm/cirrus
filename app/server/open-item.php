<?php
require_once '../../app/server/tools.php';
if(isAuthenticated()) {
	if(isset($_POST['itemPath'])) {
		if(is_file($_POST['itemPath'])) {
			if(inDataDir($_POST['itemPath']) || (inRecycleDir($_POST['itemPath']) && isOwner())) {
				$itemName = array_slice(explode( '/', $_POST['itemPath']), -1)[0];
				if($itemName !== '.perms') {
					$itemPath = $_POST['itemPath'];
					$openAs = preg_match('/pdf|image|audio|video\/.+/', mime_content_type($itemPath)) ? 'iframe' : 'download';
					$itemTempPath = getTempDir() . '/' . $itemName;
					$itemSize = filesize($itemPath) / 1024; // bytes to kilobytes
					$itemSize = $itemSize > 1024 ? 
						round(($itemSize / 1024),1) . ' Mo': // kilobytes to megabytes
						round($itemSize, 1) . ' Ko'; // kilobytes
					if(copy($itemPath, $itemTempPath)) {
						echo json_encode (
							[
								'item' => [
									'name' => $itemName,
									'openAs' => $openAs,
									'size' => $itemSize,
									'tempPath' => str_replace('../../', './', $itemTempPath),
									'uploadedOn' => date('\L\e d/m/Y \à H:i', filemtime($itemPath))
								]
							]
						);
						return;
					}
					exit(ERRORS['failure']);
				}
				exit(ERRORS['forbidden']);
			}
			exit(ERRORS['invalid']);
		}
		exit(ERRORS['missing']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);