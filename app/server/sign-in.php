<?php
require_once '../../app/server/tools.php';
$env = json_decode(file_get_contents('../../data/env.json'));
// Sign in as owner, publisher or viewer with an account.
if(
	isset($_POST['user-name'], $_POST['user-pass']) &&
	preg_match('/^[a-zA-Z0-9]{8,16}$/', $_POST['user-name']) &&
	preg_match('/^[a-zA-Z0-9.?!\-_*+=\/|\\()[\]#$@%]{8,24}$/', $_POST['user-pass']) // Note: This REGEX should match the pattern required at user registration, but remains that way for backwards compatibility reasons.
) {
	$userName = $_POST['user-name'];
	$userFilePath = null;
	$userRole = null;
	if(file_exists("../../data/users/owners/{$userName}")) {
		$userFilePath = "../../data/users/owners/{$userName}";
		$userRole = 'owner';
	}
	else if(file_exists("../../data/users/publishers/{$userName}")) {
		$userFilePath = "../../data/users/publishers/{$userName}";
		$userRole = 'publisher';
	}
	else if(file_exists("../../data/users/viewers/{$userName}")) {
		$userFilePath = "../../data/users/viewers/{$userName}";
		$userRole = 'viewer';
	}
	if($userFilePath !== null) {				
		if(password_verify($_POST['user-pass'], file_get_contents($userFilePath))) {
			require_once './JWT.php';
			$jwt = new JWT();
			$jwtLifetime = time() + (7 * 24 * 60 * 60);
			$jwtToken = $jwt->create(
				$env->cirrusId, // Secret KEY for this instance of cirrus.
				$userRole, // The role of the user between 'viewer', 'publisher' and 'owner'.
				$userName, // The name of the user for restricted area.
				$jwtLifetime // The time before this token expires.
			);	
			setcookie(
				'cirrus_user_token', // name 
				$jwtToken, // value
				$jwtLifetime, // lifetime 
				'/', // path
				$_SERVER['HTTP_HOST'], // domain
				($_SERVER['HTTP_HOST'] !== 'dev.cirrus' ? true: false), // secure in prod
				true // http only
			);
		}
		else {
			header('Location: ../../pages/sign-in/?&error=true');
			exit();
		}	
	}
	else {
		header('Location: ../../pages/sign-in/?&error=true');
		exit();
	}
	function removeDir($dir) {
		foreach(array_diff(scandir($dir), ['..', '.']) as $item) {
			$itemPath = $dir . '/' . $item;
			if(is_file($itemPath)) {
				unlink($itemPath);
			}
			else {
				removeDir($itemPath);	
				rmdir($itemPath);
			}
		}
	}
	removeDir('../../data/temp');
}
// Sign in as visitor with public access link.
else {
	if(
		isset($_GET['auth']) && 
		strlen($_GET['auth']) === 128 &&
		file_exists("../../data/auth/sign-up-as-visitor/{$_GET['auth']}")
	) {		
		require_once './JWT.php';
		$jwt = new JWT();
		$jwtLifetime = time() + (7 * 24 * 60 * 60);
		$jwtToken = $jwt->create(
			$env->cirrusId, // Secret KEY for this instance of cirrus.
			'viewer', // The role of the user between 'viewer', 'publisher' and 'owner'.
			'Anonymous', // The name of the user for restricted area.
			$jwtLifetime // The time before this token expires.
		);	
		setcookie(
			'cirrus_user_token', // name 
			$jwtToken, // value
			$jwtLifetime, // lifetime 
			'/', // path
			$_SERVER['HTTP_HOST'], // domain
			($_SERVER['HTTP_HOST'] !== 'dev.cirrus' ? true: false), // secure in prod
			true // http only
		);
	}
	else {
		header('Location: ../../pages/sign-in/?&error=true');
		exit();
	}
}
header('Location: ' . getLocation('../../'));
exit;