<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(isset($_POST['oldItemName'], $_POST['newItemName'], $_POST['parentDir'])) {
		if(inDataDir($_POST['parentDir'])) {
			$fromPath = $_POST['parentDir'] . '/' . $_POST['oldItemName'];			
			$toPath = getValidPath($_POST['parentDir'] . '/' . getValidName($_POST['newItemName']));			
			if(is_file($fromPath) || is_dir($fromPath)) {
				if(rename($fromPath, $toPath)) {
					echo json_encode(['success' => true]);
					return;
				}
				exit(ERRORS['failure']);
			}
			exit(ERRORS['missing']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);