<?php 
require_once '../../app/server/tools.php';
function searchRecursively($dir) {
	$items = array_diff(scandir($dir), ['..', '.', '.lock', '.perms']);
	foreach($items as $item) {
		$itemPath = $dir . '/' . $item;
		// Always save file match because directory access has been checked before.
		if(is_file($itemPath)) {	
			if(stripos($item, $_POST['chunckToSearch']) !== false) {
				$GLOBALS['matches'][] = [
					'label' => $item,
					'path' => $itemPath,
					'type' => 'file'
				];
			}
		}
		// Save directory match and search inside only if read is allowed.
		else if(is_dir($itemPath) && (isOwner() || isAllowed($itemPath))) {
			if(stripos($item, $_POST['chunckToSearch']) !== false) {
				$GLOBALS['matches'][] = [
					'label' => $item,
					'path' => $itemPath,
					'type' => 'subdir'
				];
			}
			searchRecursively($itemPath);
		}
	}
}
if(isAuthenticated()) {	
	if(isset($_POST['chunckToSearch'], $_POST['startLocation'])) {
		$matches[] = [
			'label' => '..',
			'path' => $_POST['startLocation'],
			'type' => 'parent'
		];
		if(inDataDir($_POST['startLocation'])) {
			searchRecursively($_POST['startLocation']);
			echo json_encode(['items' => $matches]);
			return;
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);