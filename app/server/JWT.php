<?php

Class JWT {

	private function URLBase64Encode(string $string) {
		return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($string));
	}

	public function create(string $cirrusId, string $userRole, string $userName, int $lifetime) {
		$token = [];
		$token['header'] = 
			$this->URLBase64Encode(
				json_encode(
					[
						'alg' => 'HS256',
						'typ' => 'JWT'
					]
				)
			);
		$token['payload'] = 
			$this->URLBase64Encode(
				json_encode(
					[
						'userRole' => $userRole,
						'userName' => $userName,
						'exp' => $lifetime
					]
				)	
			);
		$token['signature'] = 
			$this->URLBase64Encode(
				hash_hmac(
					'sha256',
					$token['header'] . '.' . $token['payload'],
					$cirrusId,
					true
				)
			);
		$token = $token['header'] . '.' . $token['payload'] . '.' . $token['signature'];
		return $token;
	}

	private function getPayload(string $token) {
		return json_decode(base64_decode(explode('.', $token)[1]));
	}
	public function getUserRole(string $token) {
		return $this->getPayload($token)->userRole;
	}
	public function getUserName(string $token) {
		return $this->getPayload($token)->userName;
	}

	public function verify(string $cirrusId, string $tokenToVerify) {
		$token = [];
		$token['payload'] = $this->getPayload($tokenToVerify);
		if(time() < $token['payload']->exp) {
			if($this->create(
				$cirrusId, 
				$token['payload']->userRole, 
				$token['payload']->userName, 
				$token['payload']->exp
			) === $tokenToVerify) {
				return true;
			}
		}
		return false;
	}

}