<?php 
require_once '../../app/server/tools.php';
function moveToRecycle($elm) {
	$fileName = array_slice(explode('/', $elm), -1)[0];
	$fromPath = $elm;
	$toPath = "../../data/recycle/{$fileName}";
	if(is_file($toPath) || is_dir($toPath)) {
		$basePath = $toPath; 
		$i = 1;
		while(is_file($toPath) || is_dir($toPath)) {
			$toPath = $basePath;
			$toPath .= '(' . $i . ')';
			$i++;
		}
	}
	return rename($fromPath, $toPath) ? true : false;
}

function removeDir($dir) {
	foreach(array_diff(scandir($dir), ['..', '.']) as $item) {
		$itemPath = $dir . '/' . $item;
		if(is_file($itemPath) || is_link($itemPath)) {
			unlink($itemPath);
		}
		else {
			removeDir($itemPath);	
		}
	}
	if($dir !== '../../data/recycle') {
		rmdir($dir);
	}
	return true;
}

if(isAuthenticated() && (isOwner() || isPublisher())) {
	if(isset($_POST['item'])) {
		if($_POST['item'] === 'RECYCLE') {
			$_POST['item'] = '../../data/recycle';	
		}
		if(isPublisher() && inDataDir($_POST['item'])) {
			if(is_file($_POST['item']) || is_link($_POST['item']) || is_dir($_POST['item'])) {
				if(moveToRecycle($_POST['item'])) {
					echo json_encode(['success' => true]);
					return;
				}
				exit(ERRORS['failure']);
			}
			exit(ERRORS['missing']);
		}
		else if(isOwner() && inRecycleDir($_POST['item'])) {	
			if(is_file($_POST['item']) || is_link($_POST['item'])) {
				if(unlink($_POST['item'])) {
					echo json_encode(['success' => true]);
					return;
				}
				exit(ERRORS['failure']);
			}
			else if(is_dir($_POST['item'])) {
				if(removeDir($_POST['item'])){
					echo json_encode(['success' => true]);
					return;
				}
				exit(ERRORS['failure']);
			}
			exit(ERRORS['missing']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);