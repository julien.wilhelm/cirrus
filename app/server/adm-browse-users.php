<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isOwner()) {
	$userProfiles = [
		[
			'role' => 'publisher',
			'dir' => '../../data/users/publishers'
		], 
		[
			'role' => 'viewer',
			'dir' => '../../data/users/viewers'
		]
	];
	$users = [];
	foreach($userProfiles as $userProfile) {
		$usersIn = array_diff(scandir($userProfile['dir']), ['.', '..']);
		if(count($usersIn) > 0) {
			foreach($usersIn as $userIn) {
				$users[] = [
					"role" => $userProfile['role'],
					"name" => $userIn
				];
			}
		}
	}
	if(error_get_last() === null) {
		echo json_encode (
			[
				'success' => true,
				'content' => $users
			]
		);
	}
}