<?php 
$dirPath = '../../data';
if(!is_file($dirPath . '/env.json')) {
	// Starting directories & files
	mkdir($dirPath);
	$dirsToCreate = [
		'/auth',
		'/content',
		'/recycle',
		'/temp',
		'/users',
		'/users/owners',
		'/users/publishers',
		'/users/viewers',
		'/auth/sign-up-as-owner',
		'/auth/sign-up-as-publisher',
		'/auth/sign-up-as-viewer',
		'/auth/sign-up-as-visitor' 
	];
	foreach($dirsToCreate as $dirToCreate) {
		mkdir($dirPath . $dirToCreate);
	}
	// Creates administrator account authorization
	$adminSignUpAuth = hash('sha512', 'install-start-auth');
	$cirrusId = date('Y-m-d') . '-' . hash('sha512', random_bytes(24));
	touch($dirPath . '/auth/sign-up-as-owner/' . $adminSignUpAuth);
	file_put_contents($dirPath . '/env.json', json_encode(['cirrusId' => $cirrusId]));
	// Redirects to the administrator account creation page
	header('Location: ../../pages/sign-up/?role=owner&auth=' . $adminSignUpAuth);
	exit();	
}
header('Location: ../../');
exit();