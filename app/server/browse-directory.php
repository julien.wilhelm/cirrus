<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated()) {	
	if(isset($_POST['dir'])) {
		if(inDataDir($_POST['dir']) || (inRecycleDir($_POST['dir']) && isOwner())) {
			if(is_dir($_POST['dir'])) {
				if(isOwner() || isAllowed($_POST['dir'])) {
					$items = [];
					$arrFiles = [];
					$arrLinks = [];
					$arrDirs = [];
					foreach(array_diff(scandir($_POST['dir']), ['.', '.lock', '.perms']) as $item) {
						$itemPath = $_POST['dir'] . '/' . $item;
						// Parent directory.
						if($item === '..') {
							if($_POST['dir'] !== '../../data/content' && $_POST['dir'] !== '../../data/recycle') {
								$items[] = [
									'label' => $item,
									'path' => substr($_POST['dir'], 0, strrpos($_POST['dir'], '/')),
									'type' => 'parent'
								];
							}
						}
						else {
							// Sub directory.
							if(is_dir($itemPath)) {
								if(isOwner() || isAllowed($itemPath)) {
									$arrDirs[] = $item;
								}
							}
							// Link.
							else if(is_link($itemPath)) {
								$arrLinks[] = $item;
							}
							// File.
							else {
								$arrFiles[] = $item;
							}
						}
					}
					foreach($arrDirs as $item) {
						$itemPath = $_POST['dir'] . '/' . $item;
						$items[] = [
							'label' => $item,
							'path' => $itemPath,
							'type' => 'subdir'
						];
					}
					foreach($arrLinks as $item) {
						$itemPath = $_POST['dir'] . '/' . $item;
						$items[] = [
							'label' => urldecode($item),
							'href' => readlink($itemPath),
							'path' => $itemPath,
							'type' => 'link'
						];
					}
					foreach($arrFiles as $item) {
						$itemPath = $_POST['dir'] . '/' . $item;
						$items[] = [
							'label' => $item,
							'path' => $itemPath,
							'type' => 'file'
						];
					}
					echo json_encode (['items' => $items]);
					return;
				}
				exit(ERRORS['forbidden'] . ' ' . ERRORS['redirected']);
			}
			exit(ERRORS['missing'] . ' ' . ERRORS['redirected']);
		}
		exit(ERRORS['invalid'] . ' ' . ERRORS['redirected']);
	}
	exit(ERRORS['invalid'] . ' ' . ERRORS['redirected']);
}
exit(ERRORS['forbidden']);