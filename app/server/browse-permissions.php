<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isPublisher()) {
	if(isset($_POST['dirPath'])) {
		$isRestricted = is_file($_POST['dirPath'] . '/.lock') ? true : false;
		$permsPath = $_POST['dirPath'] . '/.perms';
		$accreditedUsers = null;
		if(is_file($permsPath)) {
			$accreditedUsers = json_decode(file_get_contents($permsPath));
		}
		echo json_encode (['isRestricted' => $isRestricted, 'accreditedUsers' => $accreditedUsers]);
		return;
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);