<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated()) {
	if(setcookie('cirrus_user_token', null, -1, '/', $_SERVER['HTTP_HOST'], ($_SERVER['HTTP_HOST'] !== 'dev.cirrus' ? true: false), true)) {
		echo json_encode(['success' => true]);
		return;
	}
	exit(ERRORS['failure']);
}
exit(ERRORS['forbidden']);