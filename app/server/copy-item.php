<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated()) {
	if(isset($_POST['fromPath'], $_POST['toPath'])) {
		if(
			(inDataDir($_POST['fromPath']) && inDataDir($_POST['toPath']) && isPublisher()) ||
			(inRecycleDir($_POST['fromPath']) && inRecycleDir($_POST['toPath']) && isOwner())
		) {
			if(copy($_POST['fromPath'], getValidPath($_POST['toPath']))) {
				echo json_encode(['success' => true]);
				return;
			}
			exit(ERRORS['failure']);
		}
		exit(ERRORS['invalid']);
	}
	exit(ERRORS['invalid']);
}
exit(ERRORS['forbidden']);