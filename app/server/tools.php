<?php
ini_set('display_errors', '0');
define('VERSION', '1.9.2');
define('ERRORS', [
	'invalid' => 'Requête invalide.',
	'missing' => 'Élément introuvable.',
	'failure' => 'Échec de l\'opération.',
	'forbidden' => 'Opération non autorisée.',
	'redirected' => 'Redirection automatique.'
]);
// User / session informations
if(isset($_COOKIE['cirrus_user_token'])) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/app/server/JWT.php';
	$env = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/data/env.json'));
	$jwt = new JWT();
	$userToken = $_COOKIE['cirrus_user_token'];	
	$userRole = null;
	$userName = null;
	$isPublisher = false;
	$isOwner = false;
	$isAuthenticated = false;
	if($jwt->verify($env->cirrusId, $userToken)) {
		$userRole = $jwt->getUserRole($userToken);
		$userName = $jwt->getUserName($userToken);
		$isPublisher = ($userRole === 'owner' || $userRole === 'publisher') ? true : false;
		$isOwner = ($userRole === 'owner') ? true : false;
		$isAuthenticated = true;
	}
}
// Functions giving an access to the variables beyond the scope (return true or false)
function isOwner() { return $GLOBALS['isOwner']; }
function isPublisher() { return $GLOBALS['isPublisher']; }
function isAuthenticated() { return $GLOBALS['isAuthenticated']; }
// Functions checking the user location in folders (return true or false)
function inDataDir(string $item) { return preg_match('/^\.\.\/\.\.\/data\/content(?!\/\.+)/', $item) ? true : false; }
function inRecycleDir(string $item) { return preg_match('/^\.\.\/\.\.\/data\/recycle(?!\/\.+)/', $item) ? true : false; }
// Function checking if the user has the right to access the directory
function isAllowed($dir) {
	if(!is_file($dir . '/.lock')) {
		return true;
	}
	else {
		if(is_file($dir . '/.perms')) { 
			if(in_array($GLOBALS['userName'], json_decode(file_get_contents($dir . '/.perms')))) {
				return true;
			}
		}
	}
	return false;
}
function getValidName($name) { 
	return str_replace(["<", ">", ":", "/", "\\", "|", "?", "*", "\"", "&", "="], '-', $name); 
}
// Function renaming file/dir path to avoid overwriting existing item if necessary (renamed at the end, otherwise before the last dot)
function getValidPath($path) {
	if(is_file($path) || is_dir($path)) {
		$basePath = $path; 
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$i = 1;
		
		if($ext === '' || is_dir($path)) {
			while(is_file($path) || is_dir($path)) {
				$path = $basePath;
				$path .= '(' . $i . ')';
				$i++;
			}
		}
		else {
			$baseNoExt = substr($basePath, 0, strrpos($basePath, '.'));
			while(is_file($path)) {
				$path = $baseNoExt;
				$path .= '(' . $i . ').';
				$path .= $ext;
				$i++;
			}
		}
	}
	return $path;
}
function getLocation($location) {
	$requestURI = parse_url($_SERVER['REQUEST_URI']);
	return $location .= isset($requestURI['query']) ?
		'?' . $requestURI['query'] :
		'';
}
function getTempDir() {
	if(count(scandir('../../data/temp')) === 13) {
		function purgeDir($dir) {
			foreach(array_diff(scandir($dir), ['..', '.']) as $item) {
				$itemPath = $dir . '/' . $item;
				if(is_file($itemPath)) {
					unlink($itemPath);
				}
				else {
					purgeDir($itemPath);	
				}
			}
			if($dir !== '../../data/temp') {
				rmdir($dir);
			}
		}
		purgeDir('../../data/temp');
	}
	$tempDir = '../../data/temp' . '/' . hash('sha512', random_bytes(18));
	mkdir($tempDir);
	return $tempDir;
}