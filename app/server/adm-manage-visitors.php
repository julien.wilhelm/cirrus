<?php 
require_once '../../app/server/tools.php';
if(isAuthenticated() && isOwner()) {
	if(isset($_POST['action'])) {
		$authDirPath = '../../data/auth/sign-up-as-visitor';
		$authDirContent = scandir($authDirPath);
		$auth = isset($authDirContent[2]) ? $authDirContent[2] : null;
		$respContent = [];
		switch($_POST['action']) {
			case 'create':
			case 'browse':
				if($_POST['action'] === 'create' && $auth === null) {
					$auth = hash('sha512', random_bytes(24));
					touch("{$authDirPath}/{$auth}");
				}
				if($auth !== null) {
					$respContent = ['/?&auth=' . $auth];
				}
				break;
			case 'remove':
				if($auth !== null) {
					unlink("{$authDirPath}/{$auth}");
				}
				break;
		}
		if(error_get_last() === null) {
			echo json_encode (
				[
					'success' => true,
					'content' => $respContent
				]
			);
			return;
		}
	}
}